<?php
/*-----------------------------------------------------------------------------------

	Hello. This file contains all the important custom functions for 
	this theme. Please be very careful when editing this file.

-----------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------*/
/*	Set Content Width
/*-----------------------------------------------------------------------------------*/

if ( ! isset( $content_width ) )
	$content_width = 634;

/*-----------------------------------------------------------------------------------*/
/*	Load CSS Files
/*-----------------------------------------------------------------------------------*/

function theme_styles()  {  

	wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/css/bootstrap.css', array(), '20120208');
	wp_enqueue_style( 'style', get_template_directory_uri() . '/style.css', array(), '20120208');
	wp_enqueue_style( 'flexsliderz', get_template_directory_uri() . '/css/flexslider.css', array(), '20120208');
	wp_enqueue_style( 'isotope', get_template_directory_uri() . '/css/isotope.css', array(), '20120208');
	wp_enqueue_style( 'fanxybox', get_template_directory_uri() . '/css/jquery.fancybox.css', array(), '20120208');
	wp_enqueue_style( 'font-awesome', get_template_directory_uri() . '/css/font-awesome.min.css', array(), '20120208');
	wp_enqueue_style( 'responsive', get_template_directory_uri() . '/css/responsive.css', array(), '20120208' );
	
}
    
add_action( 'wp_enqueue_scripts', 'theme_styles' );  

/*-----------------------------------------------------------------------------------*/
/*	Load Javascript Files
/*-----------------------------------------------------------------------------------*/

function theme_scripts() {

	wp_enqueue_script( 'jquery');
	wp_enqueue_script( 'comment-reply' );
    wp_enqueue_script( 'flex-slider', get_template_directory_uri() . "/js/jquery.flexslider-min.js", array(),'', 'in_footer');	
    wp_enqueue_script( 'bootstrap', get_template_directory_uri() . "/js/bootstrap.min.js", array(),'', 'in_footer');
    wp_enqueue_script( 'jplayer', get_template_directory_uri() . "/js/jquery.jplayer.min.js", array(),'', 'in_footer');
    wp_enqueue_script( 'isotope_infinite', get_template_directory_uri() . "/js/jquery.infinitescroll.min.js", array(),'', 'in_footer');
    wp_enqueue_script( 'isotope_manual', get_template_directory_uri() . "/js/jquery.manual-trigger.js", array(),'', 'in_footer');
    wp_enqueue_script( 'isotope', get_template_directory_uri() . "/js/jquery.isotope.min.js", array(),'', 'in_footer');
    wp_enqueue_script( 'bbq', get_template_directory_uri() . "/js/jquery.ba-bbq.min.js", array(),'', 'in_footer');
    wp_enqueue_script( 'fancybox', get_template_directory_uri() . "/js/jquery.fancybox.js", array(),'', 'in_footer');
    wp_enqueue_script( 'easing', get_template_directory_uri() . "/js/easing.js", array(),'', 'in_footer');
    wp_enqueue_script( 'totop', get_template_directory_uri() . "/js/jquery.ui.totop.js", array(),'', 'in_footer');
    wp_enqueue_script( 'like_post', get_template_directory_uri().'/js/post-like.min.js', array(),'', 'in_footer');
    wp_enqueue_script( 'parallax', get_template_directory_uri().'/js/jquery.parallax-1.1.3.js', array(),'', 'in_footer');
    wp_enqueue_script( 'scrollto', get_template_directory_uri().'/js/jquery.scrollTo-1.4.2-min.js', array(),'', 'in_footer');
    wp_enqueue_script( 'scrollingp', get_template_directory_uri().'/js/jquery.scrolling-parallax.js', array(),'', 'in_footer');
    wp_enqueue_script( 'twitter_feed', get_template_directory_uri().'/twitter/jquery.tweet.js', array(),'', 'in_footer');
    wp_enqueue_script( 'isotope-relayout', get_template_directory_uri().'/js/jquery.isotope-relayout.js', array(),'', 'in_footer');

}

add_action('wp_enqueue_scripts', 'theme_scripts');

/*-----------------------------------------------------------------------------------*/
/*	Load Google Fonts
/*-----------------------------------------------------------------------------------*/

function mytheme_fonts() {
    $protocol = is_ssl() ? 'https' : 'http';
    
	wp_enqueue_style( 'mytheme-sourcesanspro', "$protocol://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,700,600" );
    	
    if (get_field('heading_font', 'option')) {
    	wp_enqueue_style( 'heading_font', "$protocol://fonts.googleapis.com/css?family=".get_field('heading_font','option').":200,300,400,700" );
    }
    
    if (get_field('body_font', 'option')) {
    	wp_enqueue_style( 'body_font', "$protocol://fonts.googleapis.com/css?family=".get_field('body_font','option').":300,400,700" );
    }
}
	   
add_action( 'wp_enqueue_scripts', 'mytheme_fonts' );

/*-----------------------------------------------------------------------------------*/
/*	Get Category Slug
/*-----------------------------------------------------------------------------------*/

function get_cat_slug($cat_id) {
	$cat_id = (int) $cat_id;
	$category = &get_category($cat_id);
	return $category->slug;
}

/*-----------------------------------------------------------------------------------*/
/*	Add Theme Support
/*-----------------------------------------------------------------------------------*/

add_theme_support( 'post-formats', array('gallery','image','link', 'quote', 'video') );
add_theme_support( 'automatic-feed-links' );
add_theme_support( 'post-thumbnails');
add_post_type_support( 'portfolio', 'post-formats' );

/*-----------------------------------------------------------------------------------*/
/*	Menu
/*-----------------------------------------------------------------------------------*/

add_action( 'init', 'register_my_menus' );
function register_my_menus() {
  register_nav_menus(
    array( 'header-menu' => 'Header Menu' )
  );
}

/*-----------------------------------------------------------------------------------*/
/*	Get Posts
/*-----------------------------------------------------------------------------------*/

function make_href_root_relative($input) {
    return preg_replace('!http(s)?://' . $_SERVER['SERVER_NAME'] . '/!', '/', $input);
}

/*-----------------------------------------------------------------------------------*/
/*	Remove title attributes
/*-----------------------------------------------------------------------------------*/

function wp_list_categories_remove_title_attributes($output) {
    $output = preg_replace('` title="(.+)"`', '', $output);
    return $output;
}
add_filter('wp_list_categories', 'wp_list_categories_remove_title_attributes');


/*-----------------------------------------------------------------------------------*/
/*	Register Widget Areas
/*-----------------------------------------------------------------------------------*/

// Footer
$side_nav = array(
	'name'          => 'footer-widgets',
	'before_widget' => '<div class="footer-widget"><div class="span3"><ul><li id="%1$s" class="widget %2$s">',
	'after_widget'  => '</li></ul></div></div>',
	'before_title'  => '<h2 class="widgettitle">',
	'after_title'   => '</h2>' );
	
register_sidebar($side_nav);

// Sidebar
$side_post = array(
	'name'          => 'post-widgets',
	'before_widget' => '<div class="sidebar-widget"><ul><li id="%1$s" class="widget %2$s">',
	'after_widget'  => '</li></ul></div>',
	'before_title'  => '<h2 class="widgettitle">',
	'after_title'   => '</h2>' );
	
	
register_sidebar($side_post);

/*-----------------------------------------------------------------------------------*/
/*	Custom Search Output
/*-----------------------------------------------------------------------------------*/

function html5_search_form( $form ) {
    $form = '<form role="search" method="get" id="searchform" action="' . home_url( '/' ) . '" >
    <input type="text" placeholder="'.__("Search").'" value="' . get_search_query() . '" name="s" id="s" />
    <input type="submit" id="searchsubmit" value="Search" />
    </form>';

    return $form;
}
add_filter( 'get_search_form', 'html5_search_form' );

/*-----------------------------------------------------------------------------------*/
/*	Add Read More to excerpt
/*-----------------------------------------------------------------------------------*/

function new_excerpt_more($more) {
	global $post;
	return '<br><br><a class="read-more" href="'. get_permalink($post->ID) . '">Read more</a>';
}
add_filter('excerpt_more', 'new_excerpt_more');

/*-----------------------------------------------------------------------------------*/
/*	Custom Comment Styling
/*-----------------------------------------------------------------------------------*/

function mytheme_comment($comment, $args, $depth) {
	$GLOBALS['comment'] = $comment;
	extract($args, EXTR_SKIP);
	
	if ( 'div' == $args['style'] ) {
		$tag = 'div';
		$add_below = 'comment';
	} else {
		$tag = 'li';
		$add_below = 'div-comment';
	} ?>
		
	<?php if ( 'div' != $args['style'] ) : ?>
	<li id="div-comment-<?php comment_ID(); ?>">
	<?php endif; ?>
		<div class="entry drop-shadow curved margin-bottom">
			<div class="avatar">
				<?php echo get_avatar( '', 50, '', '' ); ?> 
			</div>
			<div class="comment-meta">
				<h4><?php comment_author_link(); ?></h4>
				<p><?php echo human_time_diff( get_comment_time('U'), current_time('timestamp') ) . ' ago'; ?> <?php edit_comment_link('(Edit)','  ','' ); ?></p>
			</div>	
			<div class="comment-body">
		  		<?php if ($comment->comment_approved == '0') : ?>
		  			<p>Your comment is awaiting approval</p>
		  		<?php endif; ?>
		  		<?php comment_text(); ?>
		  		<?php comment_reply_link(array_merge( $args, array('add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
		  	</div>
	
		</div>
	<?php if ( 'div' != $args['style'] ) : ?>
	</li>
	<?php endif; ?>
<?php
        }

/*-----------------------------------------------------------------------------------*/
/*	Remove thumbnail dimensions
/*-----------------------------------------------------------------------------------*/

add_filter( 'post_thumbnail_html', 'remove_thumbnail_dimensions', 10, 3 );

function remove_thumbnail_dimensions( $html, $post_id, $post_image_id ) {
    $html = preg_replace( '/(width|height)=\"\d*\"\s/', "", $html );
    return $html;
}

/*-----------------------------------------------------------------------------------*/
/*	Get category slug
/*-----------------------------------------------------------------------------------*/

function get_category_slug() {
	foreach(get_the_category() as $category) {
		echo $category->slug . ' ';
	} 
}

/*-----------------------------------------------------------------------------------*/
/*	Style Switcher
/*-----------------------------------------------------------------------------------*/

function style_switcher() { ?>
	<link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/style-switcher.css"/>
	<div class="style-switcher-body"></div>
		<ul id="navigation">
			<li><span></span>
				<div id="panel">
					<p>Color theme</p>
		    		<div data-theme-path="<?php echo get_template_directory_uri() ?>" class="style-switcher-container">
		    		 <a id="style-light" class="style-color">default</a>
	    			 <a id="style-ultralight" href="#" class="style-color">ultra-light.css</a>
	    			 <a id="style-dark" class="style-color">dark.css</a>		  	     
		   			 </div>
				</div>
			</li>
		</ul>
	<script type='text/javascript' src='<?php echo get_template_directory_uri() ?>/js/style-switcher.js'></script>
<?php }

/*-----------------------------------------------------------------------------------*/
/*	Custom wp_link_pages
/*-----------------------------------------------------------------------------------*/

function custom_wp_link_pages( $args = '' ) {
	$defaults = array(
		'before' => '<p id="post-pagination"> Pages: ', 
		'after' => '</p>',
		'text_before' => '',
		'text_after' => '',
		'next_or_number' => 'number', 
		'nextpagelink' => 'Next page',
		'previouspagelink' => 'Previous page',
		'pagelink' => '%',
		'echo' => 1
	);

	$r = wp_parse_args( $args, $defaults );
	$r = apply_filters( 'wp_link_pages_args', $r );
	extract( $r, EXTR_SKIP );

	global $page, $numpages, $multipage, $more, $pagenow;

	$output = '';
	if ( $multipage ) {
		if ( 'number' == $next_or_number ) {
			$output .= $before;
			for ( $i = 1; $i < ( $numpages + 1 ); $i = $i + 1 ) {
				$j = str_replace( '%', $i, $pagelink );
				$output .= ' ';
				if ( $i != $page || ( ( ! $more ) && ( $page == 1 ) ) )
					$output .= _wp_link_page( $i );
				else
					$output .= '<span class="current-post-page">';

				$output .= $text_before . $j . $text_after;
				if ( $i != $page || ( ( ! $more ) && ( $page == 1 ) ) )
					$output .= '</a>';
				else
					$output .= '</span>';
			}
			$output .= $after;
		} else {
			if ( $more ) {
				$output .= $before;
				$i = $page - 1;
				if ( $i && $more ) {
					$output .= _wp_link_page( $i );
					$output .= $text_before . $previouspagelink . $text_after . '</a>';
				}
				$i = $page + 1;
				if ( $i <= $numpages && $more ) {
					$output .= _wp_link_page( $i );
					$output .= $text_before . $nextpagelink . $text_after . '</a>';
				}
				$output .= $after;
			}
		}
	}

	if ( $echo )
		echo $output;

	return $output;
}

/*-----------------------------------------------------------------------------------*/
/*	Enable/Disable Lightbox
/*-----------------------------------------------------------------------------------*/

function fancybox() {
	if (get_field('image_lightbox', 'option') == "enable") { 
		echo "fancybox";
	}
}

function fancybox_url($url) {
	if (get_field('image_lightbox', 'option') == "enable") { echo $url; } else { the_permalink(); }
}

/*-----------------------------------------------------------------------------------*/
/*	Custom Post Type : Portfolio
/*-----------------------------------------------------------------------------------*/

add_action('init', 'portfolio_register');
 
function portfolio_register() {
 
	$labels = array(
		'name' => 'Portfolio', 'post type general name',
		'singular_name' => 'Portfolio Item', 'post type singular name',
		'add_new' => 'Add New', 'portfolio item',
		'add_new_item' => 'Add New Portfolio Item',
		'edit_item' => 'Edit Portfolio Item',
		'new_item' => 'New Portfolio Item',
		'view_item' => 'View Portfolio Item',
		'search_items' => 'Search Portfolio',
		'not_found' =>  'Nothing found',
		'not_found_in_trash' => 'Nothing found in Trash',
		'parent_item_colon' => '',
	);
	
	$args = array(
		'labels' => $labels,
		'label' => 'asd',
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'query_var' => true,
		'menu_icon' => get_stylesheet_directory_uri() . '/img/admin_icon2.png',
		'rewrite' => true,
		'menu_position' => null,
		'supports' => array('title','comments', 'editor', 'thumbnail'),
		'show_in_menu' => TRUE,
		'show_in_nav_menus' => TRUE,
	  ); 
 
	register_post_type( 'portfolio' , $args );
	register_taxonomy("portfolio_categories", array("portfolio"), array("hierarchical" => true, "label" => "Portfolio Categories", "singular_label" => "Portfolio Category", "rewrite" => true));
	
}

/*-----------------------------------------------------------------------------------*/
/*	Custom Post Type : Gallery
/*-----------------------------------------------------------------------------------*/

add_action('init', 'gallery_register');
 
function gallery_register() {
 
	$labels = array(
		'name' => 'Gallery', 'post type general name',
		'singular_name' => 'Gallery Item', 'post type singular name',
		'add_new' => 'Add New', 'portfolio item',
		'add_new_item' => 'Add New Portfolio Item',
		'edit_item' => 'Edit Gallery Item',
		'new_item' => 'New Gallery Item',
		'view_item' => 'View Gallery Item',
		'search_items' => 'Search Gallery',
		'not_found' =>  'Nothing found',
		'not_found_in_trash' => 'Nothing found in Trash',
		'parent_item_colon' => '',
	);
	
	$args = array(
		'labels' => $labels,
		'label' => 'asd',
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'query_var' => true,
		'menu_icon' => get_stylesheet_directory_uri() . '/img/admin_icon2.png',
		'rewrite' => true,
		'menu_position' => null,
		'supports' => array('title','comments', 'editor', 'thumbnail'),
		'show_in_menu' => TRUE,
		'show_in_nav_menus' => TRUE,
	  ); 
 
	register_post_type( 'gallery' , $args );
	register_taxonomy("gallery_categories", array("gallery"), array("hierarchical" => true, "label" => "Gallery Categories", "singular_label" => "Gallery Category", "rewrite" => true));
	
}


/*-----------------------------------------------------------------------------------*/
/*	Get custom category slug name
/*-----------------------------------------------------------------------------------*/

function get_custom_category_slug($id, $category) {
	$my_terms = get_the_terms( $id, $category );

	if( $my_terms && !is_wp_error( $my_terms ) ) {
		foreach( $my_terms as $term ) {
			echo $term->slug . ' ';
		}
	} 
}

/*-----------------------------------------------------------------------------------*/
/*	Featured Header
/*-----------------------------------------------------------------------------------*/

function featured_header() {
	
	if (get_field('featured_header') == "custom_texture") { ?>
		<div class="custom-header-texture" style="background: <?php the_field('custom_texture_bg_color'); ?> url(<?php the_field('custom_texture_bg_image'); ?>);">
			<div class="container">
		  		<div class="row">
			  		<div class="span12">
				  		<?php the_field('featured_text'); ?>
				  	</div>
				</div>
			</div>
		</div>
	<?php } else if (get_field('featured_header') == "layer_slider") { ?>
 		<?php $slider_id = get_field('layerslider_id'); ?>
	 	<div class="layer-slider">
	 	<?php echo do_shortcode('[layerslider id="'.$slider_id.'"]'); ?>
	 	</div>
	 	<div class="header-preloader"></div>
	<?php } else { ?>
		<div class="default-header-texture">

		</div>
		<div class="header-preloader"></div>

	<?php
	}
}

/*-----------------------------------------------------------------------------------*/
/*	Get number of likes
/*-----------------------------------------------------------------------------------*/

function get_like_count($id) {
	if (get_post_meta($id, "votes_count")) {
		echo get_post_meta($id, "votes_count", true);
	} else {
		echo "0";
	}
}

/*-----------------------------------------------------------------------------------*/
/*	Time Ago Function
/*-----------------------------------------------------------------------------------*/

function zt_themeblvd_time_ago() {
 
	global $post;
 
	$date = get_post_time('G', true, $post);
 
	/**
	 * Where you see 'themeblvd' below, you'd
	 * want to replace those with whatever term
	 * you're using in your theme to provide
	 * support for localization.
	 */ 
 
	// Array of time period chunks
	$chunks = array(
		array( 60 * 60 * 24 * 365 , __( '', 'themeblvd' ), __( '', 'themeblvd' ) ),
		array( 60 * 60 * 24 * 30 , __( '', 'themeblvd' ), __( '', 'themeblvd' ) ),
		array( 60 * 60 * 24 * 7, __( '', 'themeblvd' ), __( '', 'themeblvd' ) ),
		array( 60 * 60 * 24 , __( '', 'themeblvd' ), __( '', 'themeblvd' ) ),
		array( 60 * 60 , __( '', 'themeblvd' ), __( '', 'themeblvd' ) ),
		array( 60 , __( '', 'themeblvd' ), __( '', 'themeblvd' ) ),
		array( 1, __( '', 'themeblvd' ), __( '', 'themeblvd' ) )
	);
 
	if ( !is_numeric( $date ) ) {
		$time_chunks = explode( ':', str_replace( ' ', ':', $date ) );
		$date_chunks = explode( '-', str_replace( ' ', '-', $date ) );
		$date = gmmktime( (int)$time_chunks[1], (int)$time_chunks[2], (int)$time_chunks[3], (int)$date_chunks[1], (int)$date_chunks[2], (int)$date_chunks[0] );
	}
 
	$current_time = current_time( 'mysql', $gmt = 0 );
	$newer_date = strtotime( $current_time );
 
	// Difference in seconds
	$since = $newer_date - $date;
 
	// Something went wrong with date calculation and we ended up with a negative date.
	if ( 0 > $since )
		return __( 'sometime', 'themeblvd' );
 
	/**
	 * We only want to output one chunks of time here, eg:
	 * x years
	 * xx months
	 * so there's only one bit of calculation below:
	 */
 
	//Step one: the first chunk
	for ( $i = 0, $j = count($chunks); $i < $j; $i++) {
		$seconds = $chunks[$i][0];
 
		// Finding the biggest chunk (if the chunk fits, break)
		if ( ( $count = floor($since / $seconds) ) != 0 )
			break;
	}
 
	// Set output var
	$output = ( 1 == $count ) ? '1 '. $chunks[$i][1] : $count . ' ' . $chunks[$i][2];
 
 
	if ( !(int)trim($output) ){
		$output = '0 ' . __( 'seconds', 'themeblvd' );
	}
 
	return $output;
}

function zt_themeblvd_time_ago2() {
 
	global $post;
 
	$date = get_post_time('G', true, $post);
 
 
	// Array of time period chunks
	$chunks = array(
		array( 60 * 60 * 24 * 365 , __( 'year', 'themeblvd' ), __( 'years', 'themeblvd' ) ),
		array( 60 * 60 * 24 * 30 , __( 'month', 'themeblvd' ), __( 'months', 'themeblvd' ) ),
		array( 60 * 60 * 24 * 7, __( 'week', 'themeblvd' ), __( 'weeks', 'themeblvd' ) ),
		array( 60 * 60 * 24 , __( 'day', 'themeblvd' ), __( 'days', 'themeblvd' ) ),
		array( 60 * 60 , __( 'hour', 'themeblvd' ), __( 'hours', 'themeblvd' ) ),
		array( 60 , __( 'minute', 'themeblvd' ), __( 'minutes', 'themeblvd' ) ),
		array( 1, __( 'second', 'themeblvd' ), __( 'seconds', 'themeblvd' ) )
	);
 
	if ( !is_numeric( $date ) ) {
		$time_chunks = explode( ':', str_replace( ' ', ':', $date ) );
		$date_chunks = explode( '-', str_replace( ' ', '-', $date ) );
		$date = gmmktime( (int)$time_chunks[1], (int)$time_chunks[2], (int)$time_chunks[3], (int)$date_chunks[1], (int)$date_chunks[2], (int)$date_chunks[0] );
	}
 
	$current_time = current_time( 'mysql', $gmt = 0 );
	$newer_date = strtotime( $current_time );
 
	// Difference in seconds
	$since = $newer_date - $date;
 
	// Something went wrong with date calculation and we ended up with a negative date.
	if ( 0 > $since )
		return __( 'sometime', 'themeblvd' );
 
	/**
	 * We only want to output one chunks of time here, eg:
	 * x years
	 * xx months
	 * so there's only one bit of calculation below:
	 */
 
	//Step one: the first chunk
	for ( $i = 0, $j = count($chunks); $i < $j; $i++) {
		$seconds = $chunks[$i][0];
 
		// Finding the biggest chunk (if the chunk fits, break)
		if ( ( $count = floor($since / $seconds) ) != 0 )
			break;
	}
 
	// Set output var
	$output = ( 1 == $count ) ? '1 '. $chunks[$i][1] : $count . ' ' . $chunks[$i][2];
 
 
	if ( !(int)trim($output) ){
		$output = '0 ' . __( 'seconds', 'themeblvd' );
	}
 
	$output .= __(' ago', 'themeblvd');
 
	return $output;
}

/*-----------------------------------------------------------------------------------*/
/*	Breadcrumbs
/*-----------------------------------------------------------------------------------*/

function the_breadcrumb() {
	echo '<div class="breadcrumbs">You are here: <ul>';
    if (!is_home()) {
        echo '<li><a href="';
        echo home_url();
        echo '">';
        echo 'Home';
        echo "</a></li>";
        if (is_category() || is_single()) {
            echo '<li>';
            the_category(' </li><li> ');
            if (is_single()) {
                echo "</li><li>";
                the_title();
                echo '</li>';
                echo '</div>';
            }
        } elseif (is_page()) {
            echo '<li>';
            echo the_title();
            echo '</li>';
            echo '</div>';
        }
    }
    elseif (is_tag()) {single_tag_title();}
    elseif (is_day()) {echo"<li>Archive for "; the_time('F jS, Y'); echo'</li>';}
    elseif (is_month()) {echo"<li>Archive for "; the_time('F, Y'); echo'</li>';}
    elseif (is_year()) {echo"<li>Archive for "; the_time('Y'); echo'</li>';}
    elseif (is_author()) {echo"<li>Author Archive"; echo'</li>';}
    elseif (isset($_GET['paged']) && !empty($_GET['paged'])) {echo "<li>Blog Archives"; echo'</li>';}
    elseif (is_search()) {echo"<li>Search Results"; echo'</li>';}
    echo '</ul>';
}

function menu_set_dropdown( $sorted_menu_items, $args ) {
    $last_top = 0;
    foreach ( $sorted_menu_items as $key => $obj ) {
        // it is a top lv item?
        if ( 0 == $obj->menu_item_parent ) {
            // set the key of the parent
            $last_top = $key;
        } else {
            $sorted_menu_items[$last_top]->classes['dropdown'] = 'dropdown';
        }
    }
    return $sorted_menu_items;
}
add_filter( 'wp_nav_menu_objects', 'menu_set_dropdown', 10, 2 );

/*-----------------------------------------------------------------------------------*/
/*	Colour Brightness
/*-----------------------------------------------------------------------------------*/

function zt_colourBrightness($hex, $percent) {
	// Work out if hash given
	$hash = '';
	if (stristr($hex,'#')) {
		$hex = str_replace('#','',$hex);
		$hash = '#';
	}
	/// HEX TO RGB
	$rgb = array(hexdec(substr($hex,0,2)), hexdec(substr($hex,2,2)), hexdec(substr($hex,4,2)));
	//// CALCULATE
	for ($i=0; $i<3; $i++) {
		// See if brighter or darker
		if ($percent > 0) {
			// Lighter
			$rgb[$i] = round($rgb[$i] * $percent) + round(255 * (1-$percent));
		} else {
			// Darker
			$positivePercent = $percent - ($percent*2);
			$rgb[$i] = round($rgb[$i] * $positivePercent) + round(0 * (1-$positivePercent));
		}
		// In case rounding up causes us to go to 256
		if ($rgb[$i] > 255) {
			$rgb[$i] = 255;
		}
	}
	//// RBG to Hex
	$hex = '';
	for($i=0; $i < 3; $i++) {
		// Convert the decimal digit to hex
		$hexDigit = dechex($rgb[$i]);
		// Add a leading zero if necessary
		if(strlen($hexDigit) == 1) {
		$hexDigit = "0" . $hexDigit;
		}
		// Append to the hex string
		$hex .= $hexDigit;
	}
	return $hash.$hex;
}

add_filter( 'the_category', 'add_nofollow_cat' );
function add_nofollow_cat( $text ) {
$text = str_replace('rel="category tag"', "", $text); return $text;
}

/*-----------------------------------------------------------------------------------*/
/*	Category Filter Custom Walker
/*-----------------------------------------------------------------------------------*/

class zt_MyWalker extends Walker_Category {
 
	function start_el(&$output, $category, $depth = 0, $args = "", $current_object_id = 0) {
		extract($args);
 
		$cat_name = esc_attr( $category->name );
		$cat_name = apply_filters( 'list_cats', $cat_name, $category );
		
		$link = '<a href="#filter=.' . $category->slug . '">' . $cat_name . '</a>'; 
		
		if ( 'list' == $args['style'] ) {
			if ( !empty($current_category) ) {
				$_current_category = get_term( $current_category, $category->taxonomy );
				if ( $category->term_id == $current_category )
					$class .=  ' current-cat';
				elseif ( $category->term_id == $_current_category->parent )
					$class .=  ' current-cat-parent';
			}
			
			$output .= "<li class=" . $class;
			$output .=  '>'.$link;
			$output .=  '';
		} else {
			$output .= $link;
		}
	}
}

if (!class_exists('WPBakeryVisualComposerAbstract')) {
  $dir = dirname(__FILE__) . '/wpbakery/';
  $composer_settings = Array(
      'APP_ROOT'      => $dir . '/js_composer',
      'WP_ROOT'       => dirname( dirname( dirname( dirname($dir ) ) ) ). '/',
      'APP_DIR'       => basename( $dir ) . '/js_composer/',
      'CONFIG'        => $dir . '/js_composer/config/',
      'ASSETS_DIR'    => 'assets/',
      'COMPOSER'      => $dir . '/js_composer/composer/',
      'COMPOSER_LIB'  => $dir . '/js_composer/composer/lib/',
      'SHORTCODES_LIB'  => $dir . '/js_composer/composer/lib/shortcodes/',
      'USER_DIR_NAME'  => 'vc_templates', /* Path relative to your current theme, where VC should look for new shortcode templates */
 
      //for which content types Visual Composer should be enabled by default
      'default_post_types' => Array('page')
  );
  require_once locate_template('/wpbakery/js_composer/js_composer.php');
  $wpVC_setup->init($composer_settings);
}


/*-----------------------------------------------------------------------------------*/
/*	Display notification for successfull installation of theme. 
/*-----------------------------------------------------------------------------------*/

add_action('admin_notices', 'install_notice');

function install_notice() {
	global $current_user ;
        $user_id = $current_user->ID;
        /* Check that the user hasn't already clicked to ignore the message */
	if ( ! get_user_meta($user_id, 'ignore_notice') ) {
        echo '<div class="updated"><p>'; 
        printf(__('<strong>Good Job!</strong> You have successfully installed Valeta. Please read documentation on getting started. | <a href="%1$s">Hide Notice</a>'), '?nag_ignore=0');
        echo "</p></div>";
	}
}

add_action('admin_init', 'nag_ignore');

function nag_ignore() {
	global $current_user;
        $user_id = $current_user->ID;
        /* If user clicks to ignore the notice, add that to their user meta */
        if ( isset($_GET['nag_ignore']) && '0' == $_GET['nag_ignore'] ) {
             add_user_meta($user_id, 'ignore_notice', 'true', true);
	}
}

/*-----------------------------------------------------------------------------------*/
/*	Load Shortcodes, Plugins, Widgets
/*-----------------------------------------------------------------------------------*/

// Add the Theme Shortcodes
include("functions/shortcodes.php");

// Add the custom jQuery
include("functions/jquery.php");

// Add the custom jQuery
include("functions/custom-css.php");

// Add custom like script
include("functions/like.php");

// Plugin Activation Class
include("functions/plugin-activate.php");

// Add custom fields and options
include("advanced-custom-fields/acf.php");
include("advanced-custom-fields/acf-fields.php");

// Visual Composer Custom
include("functions/vc-maps.php");
?>