<?php
/**
 * template name: home
 */
?>


<?php get_header();?>
<?php featured_header(); ?>

<style>
   .content-container{
      display: none !important;
   }
</style>

<?php if (get_field('breadcrumb_callout') == "Breadcrumb") { ?>
<div class="page-callout">
	<div class="container">
		<div class="row">
			<div class="span6">
				<?php the_breadcrumb(); ?>
			</div>
			
			<div class="span6">
				<div class="page-name">
					<?php the_title(); ?>
				</div>
			</div>
		</div>
	</div>
</div>
<?php } ?>
<?php if (get_field('breadcrumb_callout') == "Callout") { ?>
	<?php if (get_field('header_callout','option')) { ?>
	<div class="callout">
		<?php the_field('header_callout','option'); ?>
	</div>
	<?php } ?>
<?php } ?>


<div class="content-container">
	<div class="container">
	
		<div class="row">
			<div class="span12">
		
			<?php while (have_posts()) : the_post(); ?>
				<?php $my_terms = get_the_terms( $post->ID, 'Skills' ); ?>
				<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

					<?php the_content(); ?>
											
				</div>
			<?php endwhile; ?>
			
			</div>
		</div>
		
	</div>
</div>
<?php get_footer(); ?>   