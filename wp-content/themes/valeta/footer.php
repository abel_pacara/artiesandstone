<style>
   .sub-footer{
      display:none !important;
   }
   .copyright-text{
      text-align: center;
      margin-left: auto;
      margin-right: auto;
   }
   .social-button img{
      border-radius: 48px 48px 48px 48px;
   }
</style>
<footer>
	
	<?php if (get_field('footer_callout_section','option') == "latest_tweet") { ?>
	<div class="quote-section">
		<!-- <?php the_field('footer_callout', 'option'); ?> -->
		<a target="_blank" href="http://www.twitter.com/<?php the_field('twitter_username', 'option'); ?>" rel="alternate" data-original-title="@<?php the_field('twitter_username', 'option'); ?>" class="icon-twitter"></a>
		<div class="tweet"></div>
	</div>
	<?php } ?>
	
	<?php if (get_field('footer_callout_section','option') == "custom_content") { ?>
	<div class="quote-section">
		<?php the_field('footer_callout', 'option'); ?>
	</div>
	<?php } ?>
	
	<!-- BEGIN WIDGET FOOTER
	================================================== -->
	<div class="sub-footer">
		<div class="container">
			<div class="row">
				<?php dynamic_sidebar('footer-widgets'); ?>		
			</div>
		</div>
	</div>
	
	<!-- BEGIN FOOTER
	================================================== -->
	<div class="footer" id="as">
	    <div class="container">
	    	<div class="row">
		    	<div class="span6 copyright-text">
		    		<?php if (get_field('copyright_text', 'option')) { ?>
		    		<p><?php the_field('copyright_text', 'option')?></p>
		    		<?php } else { ?>
		    		<p>Add your footer text here through the options</p>
		    		<?php } ?>
		    	</div>
		    	<div class="span6 footer-icons">
		    	
					<ul>
					<?php if(get_field('footer_icons', 'option')) { ?>
						<?php while(has_sub_field('footer_icons', 'option')) { ?>
							
							<li class="social-button"><a href="<?php the_sub_field('footer_icon_link')?>" rel="alternate" data-original-title="<?php the_sub_field('footer_icon_title')?>"><img height="24" src="<?php echo get_template_directory_uri(); ?>/img/icons/<?php the_sub_field('footer_icon')?>" alt="" /></a></li>
				
						<?php } ?>
					<?php } ?>
				 	</ul>
		    	</div>
	    	</div>
	    </div>
	    <a rel="alternate" title="Back to top" class="up"></a>
	</div>
</footer>
<?php

/*-----------------------------------------------------------------------------------*/
/*	Google Analytics Code
/*-----------------------------------------------------------------------------------*/

$google_analytics = get_field('google_analytics', 'option'); 
$google_analytics = str_replace("&#039;","'",$google_analytics);
$google_analytics = str_replace("&lt;","<",$google_analytics); 
$google_analytics = str_replace("&quot;",'"',$google_analytics); 
$google_analytics = str_replace("&gt;",'>',$google_analytics); 
	    
echo $google_analytics;

/*-----------------------------------------------------------------------------------*/
/*	Get jQuery scripts with PHP options (functions/jquery.php)
/*-----------------------------------------------------------------------------------*/

jquery_flexslider();
jquery_contact();
jquery_isotope();
jquery_video();
jquery_audio();
jquery_removep();
jquery_tooltips();
jquery_fancybox();
jquery_dropdown();
jquery_backtotop();
jquery_custom();
?>

<script>
jQuery(window).load(function() { // makes sure the whole site is loaded
    jQuery("#container").css("opacity","1");
});


var divs = jQuery('.ls-s-1');
jQuery(window).on('scroll', function() {
   var st = jQuery(this).scrollTop();
   divs.css({ 'opacity' : (1 - st/180) });
});
</script>

<script>
		var headercontainer = jQuery(".header").height();
		
	
		
		jQuery(document).scroll(function(){
			//alert(jQuery(window).scrollTop());
		    if(jQuery(window).scrollTop() > 50)
		    {   
		        // default: padding: 40px 0 40px 0
		        jQuery('.header').addClass("header-scrolled");
		    }

		});
		
		jQuery(document).scroll(function(){

		    if(jQuery(window).scrollTop() < 50)
		    {   
		    
		    	jQuery('.header').removeClass("header-scrolled");
		    	
		    }

		});

	</script>
	
	    	    <script>
        jQuery(function($){
        jQuery(".tweet").tweet({
            username: '<?php the_field('twitter_username', 'option'); ?>',
            modpath: '<?php echo get_template_directory_uri(); ?>/twitter/',
            join_text: "auto",
            avatar_size: 32,
            count: 1,
            auto_join_text_default: "",
            auto_join_text_ed: "",
            auto_join_text_ing: "",
            auto_join_text_reply: "",
            auto_join_text_url: "",
            loading_text: "loading tweets..."
        });
    });
    </script>
    
    <script>
    
    	jQuery(window).load(function(){
			 jQuery('.isotope-preloader').css("display", "none");
			 jQuery('.isotope').css("opacity", "1");
		});
		
		
    </script>
	

<?php wp_footer(); ?>

  </body>
</html>