<?php
/*
Template Name: Portfolio
*/
?>
<?php get_header();?>
<?php  $post_count = $wp_query->found_posts; ?>

<div class="page-header">
<?php jquery_header_text(); ?>
<h1>You searched for: "<?php the_search_query() ?>"</h1>
<p>We found <?php echo $post_count; ?> posts</p>
</div>

<div class="content-container">
	<!-- Grid -->
	<div id="main-container">
		<div id="container">
			
	

			<?php while ( have_posts() ) : the_post(); ?>
	

			<?php  if(!get_post_format()) {
				get_template_part('includes/'.'standard');
			} else {
				get_template_part('includes/'.get_post_format());
			}
			?>

			<?php endwhile; // end of the loop. ?>

	
	
		</div>
	</div>
	<?php if ($post_count == 0) { ?>
	
	<div class="container">
		<div class="row">
			<div class="span12">
				<div class="entry">
					<p>Sorry, no posts were found.</p>
				</div>
			</div>
		</div>
	</div>
		
	<?php } ?>
	<div class="clearboth"></div>
	<?php if (get_next_posts_link()) { ?>
	<div class="isotope-loadmore"><?php next_posts_link('', ''); ?></div>
	<?php } ?>
</div>
<?php
//Reset Query
wp_reset_query();
?>
<?php get_footer(); ?>   