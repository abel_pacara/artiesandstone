<?php
/*
Template Name: Blog
*/
echo __FILE__;
?>
<?php get_header();?>
<?php featured_header(); ?>

<div class="page-callout">
	<div class="container">
		<div class="row">
			<div class="span6">
				<?php the_breadcrumb(); ?>
			</div>
			
			<div class="span6">
				<div class="page-name">
					<?php the_title(); ?>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="content-container">

<div class="container">
	<div class="row">
		
		<div class="span8">
					
			<?php
         $cat_events_id = get_cat_ID("events"); //105;
         $cat_news_id = get_cat_ID("news"); //106;
         
         
			$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
         
         /*
			query_posts(array('post_type'=>'post', 'paged' => $paged, 'posts_per_page' => get_option('posts_per_page'),
                           "category_in"=>array($cat_events_id,$cat_news_id)));
         
          */
         
         global $wp_query;
         
         
         $args = array( 
                  'post_type'=>'post',
                  'paged' => $paged,
                  'posts_per_page' => get_option('posts_per_page'),
                  'category__in' => array($cat_events_id,$cat_news_id),
                  );
         
         $wp_query = new WP_Query($args);
          
         ?>
			
			<?php while ( have_posts() ) : the_post(); ?>
			
			<div class="row">
				
<!--
				<div class="span1">
					<div class="entry-blog-time">
						<div class="date">
							<span class="time"> <?php echo zt_themeblvd_time_ago(); ?> </span>
							<span class="ago"><?php echo preg_replace("/[^a-zA-Z\s]/", "", zt_themeblvd_time_ago2()); ?></span>
						</div>
					</div>
				</div>
				
-->
				<div class="span8">
							
					<?php
					global $more;
					$more = 0;
					?>
					
					<div class="classic">
						<?php if(!get_post_format()) {
							get_template_part('includes/'.'standard');
						} else {
							get_template_part('includes/'.get_post_format());
						}
						?>
					</div>
				
				</div>
				
			</div>
			<?php endwhile; // end of the loop. ?>
			<div class="pagination"><p><?php posts_nav_link('&nbsp;','Previous page','Next page'); ?></p></div>
		</div>
						
		<div class="span4">
			<?php dynamic_sidebar('post-widgets'); ?>
		</div>	
		
	</div>
</div>



</div>

<?php wp_reset_query(); ?>
<?php get_footer(); ?>   