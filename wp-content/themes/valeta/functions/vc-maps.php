<?php


wpb_map( array(
   "name" => __("Team Member"),
   "base" => "team_member",
   "class" => "",
   "category" => __('Content'),
   'admin_enqueue_js' => array(get_template_directory_uri().'/vc_extend/bartag.js'),
   'admin_enqueue_css' => array(get_template_directory_uri().'/vc_extend/bartag.css'),
   "params" => array(
      array(
         "type" => "textfield",
         "holder" => "div",
         "class" => "",
         "heading" => __("Name"),
         "param_name" => "name",
         "value" => __("John Smith"),
         "description" => __("Enter the team members name")
      ),
      array(
         "type" => "textfield",
         "holder" => "div",
         "class" => "",
         "heading" => __("Position"),
         "param_name" => "position",
         "value" => __("CEO"),
         "description" => __("Enter the team members position")
      ),
      array(
         "type" => "attach_image",
         "holder" => "div",
         "class" => "",
         "heading" => __("Image"),
         "param_name" => "image_src",
         "description" => __("Upload an image")
      ),
       array(
         "type" => "textarea",
         "holder" => "div",
         "class" => "",
         "heading" => __("Description"),
         "param_name" => "description",
         "description" => __("Write a description")
      ),
       array(
         "type" => "exploded_textarea",
         "holder" => "div",
         "class" => "",
         "heading" => __("Social Buttons"),
         "param_name" => "social_buttons",
         "value" => __("http://www.facebook.com|icon-facebook-sign,http://www.twitter.com|icon-twitter-sign,http://www.pinterest.com|icon-pinterest-sign"),
         "description" => __("Input social buttons here. Divide URL and Icon name with line break(enter). Example: http://www.facebook.com|icon-facebook-sign <br> Visit this page, for a full list of icons: <a href='http://fortawesome.github.io/Font-Awesome/icons/'>http://fortawesome.github.io/Font-Awesome/icons/</a>")
      )
   )
) );

wpb_map( array(
   "name" => __("Pricing Table"),
   "base" => "pricing_table",
   "class" => "",
   "category" => __('Content'),
   'admin_enqueue_js' => array(get_template_directory_uri().'/vc_extend/bartag.js'),
   'admin_enqueue_css' => array(get_template_directory_uri().'/vc_extend/bartag.css'),
   "params" => array(
      array(
         "type" => "textfield",
         "holder" => "div",
         "class" => "",
         "heading" => __("Price"),
         "param_name" => "price",
         "value" => __("$|20"),
         "description" => __("Enter the currency and price for this package. e.g $|20")
      ),
      array(
         "type" => "textfield",
         "holder" => "div",
         "class" => "",
         "heading" => __("Cycle"),
         "param_name" => "cycle",
         "value" => __("month"),
         "description" => __("Enter the billing cycle. e.g month/year/lifetime")
      ),
      array(
         "type" => "textfield",
         "holder" => "div",
         "class" => "",
         "heading" => __("Package"),
         "param_name" => "package",
         "value" => __("Starter"),
         "description" => __("Enter a name for this package")
      ),
      array(
         "type" => "textfield",
         "holder" => "div",
         "class" => "",
         "heading" => __("Package Tag"),
         "param_name" => "package_tag",
         "value" => __("Popular"),
         "description" => __("Add a tag beside the package name. e.g Popular. Leave blank for no tag.")
      ),
      array(
         "type" => "exploded_textarea",
         "holder" => "div",
         "class" => "",
         "heading" => __("Features"),
         "param_name" => "features",
         "description" => __("Enter the features for this package. Divide each feature with a line break(enter)")
      ),
      array(
         "type" => "textfield",
         "holder" => "div",
         "class" => "",
         "heading" => __("Button Value"),
         "param_name" => ("button_value"),
         "value" => __("Sign up now"),
         "description" => __("Enter a value for the button")
      ),
      array(
         "type" => "textfield",
         "holder" => "div",
         "class" => "",
         "heading" => __("Link"),
         "param_name" => "button_link",
         "description" => __("Enter a link which you would like the button to point to")
      )
   )
) );
wpb_map( array(
   "name" => __("Contact Form"),
   "base" => "contact_form",
   "class" => "",
   "category" => __('Content'),
   'admin_enqueue_js' => array(get_template_directory_uri().'/vc_extend/bartag.js'),
   'admin_enqueue_css' => array(get_template_directory_uri().'/vc_extend/bartag.css'),
) );
wpb_map( array(
   "name" => __("Features"),
   "base" => "features",
   "class" => "",
   "category" => __('Content'),
   'admin_enqueue_js' => array(get_template_directory_uri().'/vc_extend/bartag.js'),
   'admin_enqueue_css' => array(get_template_directory_uri().'/vc_extend/bartag.css'),
   "params" => array(
      array(
         "type" => "textfield",
         "holder" => "div",
         "class" => "",
         "heading" => __("Icon"),
         "param_name" => "icon",
         "value" => __("icon-bolt"),
         "description" => __("Specify an icon name for this feature. for a full list of icon names: <a href='http://fortawesome.github.io/Font-Awesome/icons/'>http://fortawesome.github.io/Font-Awesome/icons/</a>")
      ),
      array(
         "type" => "textfield",
         "holder" => "div",
         "class" => "",
         "heading" => __("Title"),
         "param_name" => "title",
         "value" => __("Super Fast"),
         "description" => __("Specify a title for this feature")
      ),
      array(
         "type" => "textarea",
         "holder" => "div",
         "class" => "",
         "heading" => __("Description"),
         "param_name" => "description",
         "value" => __("This is an awesome description of this awesome feature."),
         "description" => __("Add a description for this feature")
      )
   )
) );
wpb_map( array(
   "name" => __("Recent Work"),
   "base" => "recent_work",
   "class" => "",
   "category" => __('Content'),
   'admin_enqueue_js' => array(get_template_directory_uri().'/vc_extend/bartag.js'),
   'admin_enqueue_css' => array(get_template_directory_uri().'/vc_extend/bartag.css'),
    "params" => array(
      array(
         "type" => "textfield",
         "holder" => "div",
         "class" => "",
         "heading" => __("Title"),
         "param_name" => "title",
         "value" => __("recent work"),
         "description" => __("Specify a title for this feature")
      ),
      array(
         "type" => "textfield",
         "holder" => "div",
         "class" => "",
         "heading" => __("Title Link"),
         "param_name" => "title_link",
         "value" => __("view all work"),
         "description" => __("Add a link in the title")
      ),
      array(
         "type" => "textfield",
         "holder" => "div",
         "class" => "",
         "heading" => __("Title Link Path"),
         "param_name" => "title_link_href",
         "value" => __("http://www.themeforest.net"),
         "description" => __("Specify the url you would like the title link to go to")
      )
   )

) );
wpb_map( array(
   "name" => __("Recent Blog Posts"),
   "base" => "recent_posts",
   "class" => "",
   "category" => __('Content'),
   'admin_enqueue_js' => array(get_template_directory_uri().'/vc_extend/bartag.js'),
   'admin_enqueue_css' => array(get_template_directory_uri().'/vc_extend/bartag.css'),
    "params" => array(
      array(
         "type" => "textfield",
         "holder" => "div",
         "class" => "",
         "heading" => __("Title"),
         "param_name" => "title",
         "value" => __("recent work"),
         "description" => __("Specify a title for this feature")
      ),
      array(
         "type" => "textfield",
         "holder" => "div",
         "class" => "",
         "heading" => __("Title Link"),
         "param_name" => "title_link",
         "value" => __("view all work"),
         "description" => __("Add a link in the title")
      ),
      array(
         "type" => "textfield",
         "holder" => "div",
         "class" => "",
         "heading" => __("Title Link Path"),
         "param_name" => "title_link_href",
         "value" => __("http://www.themeforest.net"),
         "description" => __("Specify the url you would like the title link to go to")
      )
   )

) );
wpb_map( array(
   "name" => __("Bordered Content"),
   "base" => "bordered_content",
   "class" => "",
   "category" => __('Content'),
   'admin_enqueue_js' => array(get_template_directory_uri().'/vc_extend/bartag.js'),
   'admin_enqueue_css' => array(get_template_directory_uri().'/vc_extend/bartag.css'),
    "params" => array(
      array(
         "type" => "textarea_html",
         "holder" => "div",
         "class" => "",
         "heading" => __("Content"),
         "param_name" => "content",
         "description" => __("Add your content")
      ),
   )

) );
wpb_map( array(
   "name" => __("Parallax Section"),
   "base" => "parallax_section",
   "class" => "",
   "category" => __('Content'),
   'admin_enqueue_js' => array(get_template_directory_uri().'/vc_extend/bartag.js'),
   'admin_enqueue_css' => array(get_template_directory_uri().'/vc_extend/bartag.css'),
    "params" => array(
      array(
         "type" => "textarea_html",
         "holder" => "div",
         "class" => "",
         "heading" => __("Content"),
         "param_name" => "content",
         "description" => __("Add your content")
      ),
      array(
         "type" => "attach_image",
         "holder" => "div",
         "class" => "",
         "heading" => __("Image"),
         "param_name" => "background",
         "description" => __("Upload a background image")
      )
   )

) );
wpb_map( array(
   "name" => __("Client Logo"),
   "base" => "client_logo",
   "class" => "",
   "category" => __('Content'),
   'admin_enqueue_js' => array(get_template_directory_uri().'/vc_extend/bartag.js'),
   'admin_enqueue_css' => array(get_template_directory_uri().'/vc_extend/bartag.css'),
    "params" => array(
      array(
         "type" => "attach_image",
         "holder" => "div",
         "class" => "",
         "heading" => __("Image"),
         "param_name" => "image",
         "description" => __("Upload a logo")
      )
   )

) );
wpb_map( array(
   "name" => __("Full Width Section"),
   "base" => "fullwidth",
   "class" => "",
   "category" => __('Content'),
   'admin_enqueue_js' => array(get_template_directory_uri().'/vc_extend/bartag.js'),
   'admin_enqueue_css' => array(get_template_directory_uri().'/vc_extend/bartag.css'),
    "params" => array(
      array(
         "type" => "textarea_html",
         "holder" => "div",
         "class" => "",
         "heading" => __("Content"),
         "param_name" => "content",
         "description" => __("Add your content")
      ),
      array(
         "type" => "color_picker",
         "holder" => "div",
         "class" => "",
         "heading" => __("Background Color"),
         "param_name" => "background_color",
         "description" => __("Specify a background color")
      )
   )

) );


?>