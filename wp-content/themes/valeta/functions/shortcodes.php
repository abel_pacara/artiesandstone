<?php

/*-----------------------------------------------------------------------------------*/
/*	Boxed
/*-----------------------------------------------------------------------------------*/

function boxed( $atts, $content = null ) {  
    return '<span class="boxed">'.do_shortcode($content).'</span>';  
}
add_shortcode("boxed", "boxed");

/*-----------------------------------------------------------------------------------*/
/*	Team
/*-----------------------------------------------------------------------------------*/

function team_member( $atts, $content = null ) {
	extract(shortcode_atts( array(
							'image_src' => '',
							'name' => '',
							'position' => '',
							'description' => '',
							'social_buttons' => '',
							), $atts));   
							
	$link_to = wp_get_attachment_image_src( $image_src, 'large');
	
	$graph_lines = explode(",", $social_buttons);
	
	$output = "";
	foreach ($graph_lines as $line) {
	
	    $single_val = explode("|", $line);
	    $output .= "<a href='$single_val[0]' class='$single_val[1]'></a>";
	    
	}

	
    return "<a class='entry-image'><img src='$link_to[0]' alt='' /></a><div class='entry'><h1 class='heading'>$name</h1><div class='post-meta'>$position</div><div class='team-icons'>$output</div><p>$description</p></div>";  
}

add_shortcode("team_member", "team_member");

/*-----------------------------------------------------------------------------------*/
/*	Team
/*-----------------------------------------------------------------------------------*/

function progress_bar( $atts, $content = null ) {
	extract(shortcode_atts( array(
							'progress_type' => '',
							'progress' => '',
							), $atts));   
    return '<div class="progress progress-'.$progress_type.'"><div class="bar" style="width: '.$progress.'"></div></div>';  
}
add_shortcode("progress_bar", "progress_bar");

/*-----------------------------------------------------------------------------------*/
/*	Icons
/*-----------------------------------------------------------------------------------*/

function icon( $atts) {
	extract(shortcode_atts( array(
							'name' => '',
							'size' => '',
							'colour' => '',
							), $atts));   
    return '<i class="'.$name.'" style="font-size:'.$size.'; color: '.$colour.'"></i>';  
}
add_shortcode("icon", "icon");

/*-----------------------------------------------------------------------------------*/
/*	Buttons
/*-----------------------------------------------------------------------------------*/

function button( $atts, $content = null ) {
	extract(shortcode_atts( array(
							'size' => '',
							'href' => '',
							'color' => '',
							), $atts));
	
		 if (get_field('theme_color', 'option')) {
   		$theme_color = $color;
   	}
   	
		$dark_color = zt_colourBrightness($theme_color,-0.8);
		$slight_dark_color = zt_colourBrightness($theme_color,-0.9);
		$light_color = zt_colourBrightness($theme_color,0.65);		
				
		if ($color) {		
   		$button_color = 'style="background: '.$theme_color.' !important; border: 1px solid '.$dark_color.' !important; text-shadow: 1px 1px '.$dark_color.' !important; box-shadow:inset 0px 1px 0px 0px '.$light_color.' !important;"';
   		} else {
	   		$button_color = "";
   		}
   		
    return '<a href="'.$href.'" '.$button_color.' class="button '.$size.'">'.do_shortcode($content).'</a>';  
}
add_shortcode("button", "button");

/*-----------------------------------------------------------------------------------*/
/*	Full width section
/*-----------------------------------------------------------------------------------*/

function fullwidth( $atts, $content = null ) {
	extract(shortcode_atts( array(
							'background_color' => '',
							), $atts));   
    return '<div style="background-color: '.$background_color.';" class="fullwidth-section">'.do_shortcode($content).'</div>';  
}
add_shortcode("fullwidth", "fullwidth");

/*-----------------------------------------------------------------------------------*/
/*	Buttons Mini
/*-----------------------------------------------------------------------------------*/

function btn_mini( $atts, $content = null ) {
	extract(shortcode_atts( array(
							'btn_color' => '',
							), $atts));   
    return '<div class="button-mini '.$btn_color.'">'.do_shortcode($content).'</div>';  
}
add_shortcode("btn_mini", "btn_mini");

/*-----------------------------------------------------------------------------------*/
/*	Buttons Mini
/*-----------------------------------------------------------------------------------*/

function hr() {
    return '<hr>';  
}
add_shortcode("hr", "hr");

/*-----------------------------------------------------------------------------------*/
/*	Contact form
/*-----------------------------------------------------------------------------------*/

function contact_form() {
	return '<div class="contact-alerts"></div>
					
				<!-- Contact Form  -->
				<form id="contact" action="contact.php">
					<div class="row-fluid">
						<div class="span6">
							<input id="name" type="text" value="" placeholder="Name" name="name">
						</div>	
						<div class="span6">
							<input id="email" type="text" value="" placeholder="Email" name="email">
						</div>	
					</div>			
					
					<div class="row-fluid">
						<div class="span12">
							<textarea id="message" required="" rows="6" placeholder="Message" name="message"></textarea>
						</div>
					</div>
					
					<a class="contact-submit" href="#">Submit</a>
				</form>';
}

add_shortcode("contact_form", "contact_form");

/*-----------------------------------------------------------------------------------*/
/*	Tabs
/*-----------------------------------------------------------------------------------*/

function tabgroup( $atts, $content ){
	$GLOBALS['tab_count'] = 0;

	do_shortcode( $content );

	if( is_array( $GLOBALS['tabs'] ) ){
		foreach( $GLOBALS['tabs'] as $tab ){
			$tabs[] = '<li class="'.$tab['state'].'"><a href="#'.$tab['title'].'" data-toggle="tab">'.$tab['title'].'</a></li>';
			$panes[] = '<div id="'.$tab['title'].'" class="'.$tab['state'].' tab-pane"><p>'.$tab['content'].'</p></div>';
			}
			$return = "\n".'<ul class="tabs" id="myTab">'.implode( "\n", $tabs ).'</ul>'."\n".'<div class="tab-content">'.implode( "\n", $panes ).'</div>'."\n";
		}
		return $return;
	}
add_shortcode( 'tabgroup', 'tabgroup' );
	
function tabs( $atts, $content ){
	extract(shortcode_atts(array(
	'title' => 'Tab %d',
	'state' => ''
), $atts));

$x = $GLOBALS['tab_count'];
$GLOBALS['tabs'][$x] = array( 'title' => sprintf( $title, $GLOBALS['tab_count'] ), 'state' => sprintf( $state, $GLOBALS['tab_count'] ), 'content' =>  $content );

$GLOBALS['tab_count']++;
}
add_shortcode( 'tab', 'tabs' );

/*-----------------------------------------------------------------------------------*/
/*	Pricing Table
/*-----------------------------------------------------------------------------------*/


function pricing_table($atts) {
	
	extract(shortcode_atts(array(
		'price' => '',
		'cycle' => '',
		'package' => '',
		'package_tag' => '',
		'features' => '',
		'button_value' => '',
		'button_link' => '',
	), $atts));
	
	$features_line = explode(",", $features);
	
	$features_output = "";
	foreach ($features_line as $line) {

   		$features_output .= "<li>".$line."</li>";
   	}
   	
   	$price_val = explode("|", $price);
   	
   	$package_tag_output = "";
   	if ($package_tag) {
   		$package_tag_output = '<span>'.$package_tag.'</span>';
   	}
   	
    return
    
		'<div class="package">
			
			<div class="price">
				<span class="currency">'.$price_val[0].'</span><span class="amount">'.$price_val[1].'</span><p>/ '.$cycle.'</p>
			</div>
			<div class="package-name">
				<h2>'.$package.$package_tag_output.'</h2>
			</div>
			<ul>
			'.$features_output.'
			</ul>
			<div class="buy-button">
				<a href="'.$button_link.'">'.$button_value.'</a>
			</div>
		
		</div>
		';
     
}
add_shortcode("pricing_table", "pricing_table");

/*-----------------------------------------------------------------------------------*/
/*	Features
/*-----------------------------------------------------------------------------------*/

function features($atts) {

	extract(shortcode_atts(array(
		'icon' => '',
		'title' => '',
		'description' => ''
		
	), $atts));
	
	return '<div class="features">
							<div class="features-icon"><i class="'.$icon.'"></i></div><br><br>
							<h2>'.$title.'</h2>
							<p>'.$description.'</p>
						</div>';
}

add_shortcode("features", "features");

/*-----------------------------------------------------------------------------------*/
/*	Bordered Content
/*-----------------------------------------------------------------------------------*/

function bordered_content($atts, $content = null) {

	
	return '<div style="" class="bordered-content">'.$content.'</div>';
}

add_shortcode("bordered_content", "bordered_content");

/*-----------------------------------------------------------------------------------*/
/*	Parallax Section
/*-----------------------------------------------------------------------------------*/

function parallax_section($atts, $content = null) {

	extract(shortcode_atts(array(
		'background' => ''
		
	), $atts));


	$background_src = wp_get_attachment_image_src( $background, 'full');
	
	return '<div class="parallax-section" style="background-image: url('.$background_src[0].');">'.do_shortcode($content).'</div>';
}

add_shortcode("parallax_section", "parallax_section");

/*-----------------------------------------------------------------------------------*/
/*	Client Logo
/*-----------------------------------------------------------------------------------*/

function client_logo($atts, $content = null) {

	extract(shortcode_atts(array(
		'image' => ''
		
	), $atts));


	$image_src = wp_get_attachment_image_src( $image, 'full');
	
	return '<img class="client-logo" src="'.$image_src[0].'" alt="" />';
}

add_shortcode("client_logo", "client_logo");

/*-----------------------------------------------------------------------------------*/
/*	Recent Work
/*-----------------------------------------------------------------------------------*/

function recent_work($atts) { 
	
		extract(shortcode_atts(array(
		'title' => '',
		'title_link' => '',
		'title_link_href' => ''
		
	), $atts));
	
	
?>
		
	<?php 
	
	$paged = (get_query_var('page')) ? get_query_var('page') : 1;
		query_posts(array('post_type'=>'portfolio', 'paged' => $paged, 'posts_per_page' => '3'));
				$output = '';
	?>
	
		
	<?php while ( have_posts() ) : the_post(); ?>

		<?php
		global $more;
		$more = 0;
		
		ob_start();  
		get_template_part('includes/'.get_post_format());

		$get_template_part = ob_get_clean();    
		
		
		$output .= '<div class="isotope-post span4">'.$get_template_part.'</div>';
		if ($title) {
		
			if ($title_link) {
				$title_link_output = '<a href="'.$title_link_href.'">:&nbsp; '.$title_link.'</a>';
			}
			
			$title_output = '<h3 class="content-heading">
				<span>recent work &nbsp;'.$title_link_output.'</span>
				</h3>';
		}
		?>
	<?php endwhile; // end of the loop. ?>
			
	<?php wp_reset_query(); ?>
			
	<?php return $title_output.'<div class="isotope row">'.$output.'</div>'; ?>

<?php }

add_shortcode("recent_work", "recent_work");

/*-----------------------------------------------------------------------------------*/
/*	Recent Blog Posts
/*-----------------------------------------------------------------------------------*/

function recent_posts($atts) { 
	
		extract(shortcode_atts(array(
		'title' => '',
		'title_link' => '',
		'title_link_href' => ''
		
	), $atts));
	
	
?>
		
	<?php $paged = (get_query_var('page')) ? get_query_var('page') : 1;
	query_posts(array('post_type'=>'post', 'paged' => $paged, 'posts_per_page' => '3'));
	?>

	<?php while ( have_posts() ) : the_post(); ?>

		<?php
		global $more;
		$more = 0;
		
		ob_start();  
		get_template_part('includes/'.get_post_format());

		$get_template_part = ob_get_clean();      
		
		$output .= '<div class="isotope-post span4">'.$get_template_part.'</div>';
		if ($title) {
		
			if ($title_link) {
				$title_link_output = '<a href="'.$title_link_href.'">:&nbsp; '.$title_link.'</a>';
			}
			
			$title_output = '<h3 class="content-heading">
				<span>recent posts &nbsp;'.$title_link_output.'</span>
				</h3>';
		}
		?>
	<?php endwhile; // end of the loop. ?>
	
	<?php wp_reset_query(); ?>
	
	<?php return $title_output.'<div class="isotope row">'.$output.'</div>'; ?>

<?php }

add_shortcode("recent_posts", "recent_posts");


?>