<?php

/*-----------------------------------------------------------------------------------*/
/*	This file is for custom styling options. Use the options panel to apply
/*  the settings. Editing this file could break the theme.
/*-----------------------------------------------------------------------------------*/

function my_styles_method() {

	wp_enqueue_style(
		'custom-style',
		get_template_directory_uri() . '/css/custom.css'
	);
	
    $background_color = get_field('background_color', 'option'); // Background Color
    $background_image = get_field('upload_background', 'option'); // Background Color
    $heading_font = get_field('heading_font', 'option');
    $body_font = get_field('body_font', 'option');   
    $body_font_size = get_field('body_font_size', 'option');
    $line_height = (get_field('body_font_size', 'option') * 1.65)."px";
    $custom_css_code = get_field('custom_css', 'option');
    
    // If custom color isn't set, use default color #ef6957
    
    if (get_field('theme_color', 'option')) {
   		$theme_color = get_field('theme_color', 'option');
   	} else {
	   	$theme_color = '#ef6957';
   	}
    
    // CSS
    
    $custom_css = "";
    
    if ($background_color or $background_image) {
    	$custom_css = "body { background: $background_color url($background_image) }";
    }
    
    if ($heading_font) {
   		$custom_css .= "h1, h2, h3, h4, h5, h6 { font-family: $heading_font !important; }";
    }
    
    if ($body_font) {
   		$custom_css .= "p { font-family: $body_font !important; }";
    }
    
    if ($body_font_size) {
   		$custom_css .= "p { font-size: $body_font_size; line-height: $line_height; }";
    }
    
    if ($theme_color) {
	    
	  	
		$dark_color = zt_colourBrightness($theme_color,-0.8);
		$slight_dark_color = zt_colourBrightness($theme_color,-0.9);
		$light_color = zt_colourBrightness($theme_color,0.65);		
				
   		$custom_css .= ".button, .wpb_button { background: $theme_color !important; border: 1px solid $dark_color !important; text-shadow: 1px 1px $dark_color !important; box-shadow:inset 0px 1px 0px 0px $light_color !important;}";
   		
   		$custom_css .= ".button:hover, .wpb_button:hover { background: $slight_dark_color !important; border: 1px solid $dark_color; }";
   		
   		$custom_css .= ".content-heading a { color: $theme_color; }";
   		
   		$custom_css .= ".post-icons a:hover, .post-icons a:hover [class*='icon'] { color: $theme_color !important; }";
   		
   		$custom_css .= ".header-scrolled .navigation .current-menu-item a { background: $theme_color !important; }";
   		
   		$custom_css .= ".header-scrolled .navigation li a:hover { background: $theme_color; }";
   		
   		$custom_css .= ".features-icon, .features-readmore:hover { color: $theme_color; }";
   		
   		$custom_css .= ".quote-section [class*='icon-'] { color: $theme_color; }";
   		
   		$custom_css .= ".wpb_tabs .ui-tabs-active .ui-tabs-anchor:after { border-top: 8px solid $theme_color; }";
   		
   		
   		$custom_css .= ".wpb_tabs .ui-tabs-active .ui-tabs-anchor { background-color: $theme_color; border-top: 1px solid $theme_color !important; border-bottom: 1px solid $theme_color !important; }";
   		
   		$custom_css .= ".vc_bar { background: $theme_color !important; }";
   		
   		$custom_css .= ".team-icons [class*='icon-']:hover { color: $theme_color; }";
   		
   		$custom_css .= ".package-name span { background: $theme_color; }";
   		
   		$custom_css .= ".buy-button a:hover { background: $theme_color; }";
   		
   		$custom_css .= "#submit:hover, .contact-submit:hover { background: $theme_color }";
   		
   		$custom_css .= ".read-more:hover { border: 1px solid $theme_color; color: $theme_color !important; }";
   		
   		$custom_css .= ".entry-quote, .entry-link { background: $theme_color; }";
   		
   		$custom_css .= ".view [class*='icon'] { background: $theme_color; }";
   		
   		$custom_css .= ".wpb_tour .ui-tabs-active .ui-tabs-anchor { background: $theme_color; border-left: 1px solid $theme_color !important; border-right: 1px solid $theme_color !important; }";
   		
   		$custom_css .= ".heading a:hover { color: $theme_color; }";
   		
   		$custom_css .= ".tweet_text a { color: $theme_color; }";
   		
   		$custom_css .= ".isotope-loadmore a { background: $theme_color; }";
   		
    }
    
    wp_add_inline_style( 'custom-style', $custom_css );
}

add_action( 'wp_enqueue_scripts', 'my_styles_method' );

?>