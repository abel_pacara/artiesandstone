<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="utf-8">
    <title><?php bloginfo('name'); ?> <?php wp_title(); ?></title>  
    <meta name="description" content="<?php the_field('meta_description', 'option'); ?>">
    <meta name="keywords" content="<?php the_field('meta_keywords', 'option'); ?>">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    
    <!-- Apple Touch Icon -->
    <?php if (get_field('apple_touch_icon', 'option')) { ?>
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php the_field('apple_touch_icon', 'option'); ?>">
    <?php } ?>
    
    <!--  Favicon -->
    <?php if (get_field('favicon', 'option')) { ?>
    <link rel="shortcut icon" href="<?php the_field('favicon', 'option'); ?>">
    <?php } ?>

    <?php 
    if ( is_singular() && get_option( 'thread_comments' ) ) 	wp_enqueue_script( 'comment-reply' );
    wp_head();
    ?>
</head>

<body <?php body_class(); ?>>
	
	<div class="preloader"></div>

	<!-- BEGIN Mobile Navigation -->
	<div class="mobile-nav-container">
		<div class="mobile-nav-bar">
		    <button type="button" class="btn-mobile-nav" data-toggle="collapse" data-target="#mobile-nav"><i class="icon-plus"></i></button>
			
			<!-- Mobile Logo -->
			<?php if (get_field('mobile_logo', 'option')) { ?>
			<div class="mobile-logo">
				<a href="<?php echo site_url(); ?>">
					<img src="<?php the_field('mobile_logo', 'option'); ?>" alt=""/>	
				</a>
			</div>
			<?php } else { ?>
			<div class="mobile-logo">
				<a href="<?php echo site_url(); ?>">
					<img src="<?php the_field('logo_image', 'option'); ?>" alt=""/>	
				</a>
			</div>
			<?php } ?>
			
		</div>
		
		<!-- Mobile Drop Down -->
		<div id="mobile-nav" class="collapse">
			<ul>
				<?php
				$defaults = array(
			   	'theme_location'  => '',
			   	'container'       => 'ul',
			   	'menu_class'      => 'menu',
			   	'echo'            => true,
			   	'fallback_cb'     => 'wp_page_menu',
			   	'items_wrap'      => '%3$s',
			   	'depth'           => 0
			   );
			   ?>
			   <?php wp_nav_menu( $defaults ); ?>
			</ul>
		</div>
	</div>
	<!-- END Mobile Navigation -->
	
	<img class="hidden" src="<?php echo get_template_directory_uri(); ?>/img/nav-scrolled-bg.png"/>
	<div class="header">
		
		<div class="navigation-container">
			<div class="container">
				<div class="row">
					<div class="span12">
	
						<div class="header-navigation">
							<div class="header-logo">
								<a href="<?php echo site_url(); ?>">
									<img src="<?php the_field('logo_image', 'option'); ?>" alt=""/>
								</a>
							</div>
							<div class="header-mobile-logo">
								<a href="<?php echo site_url(); ?>">
									<img src="<?php the_field('mobile_logo', 'option'); ?>" alt=""/>
								</a>
							</div>
		
							<ul class="navigation">
								<?php
								   $the_menu = array(
								   	'theme_location'  => '',
								   	'container'       => 'ul',
								   	'menu_class'      => 'menu',
								   	'echo'            => true,
								   	'fallback_cb'     => 'wp_page_menu',
								   	'items_wrap'      => '%3$s',
								   	'depth'           => 0
								   );
								   
								   wp_nav_menu( $the_menu );
								?>
								
							</ul>
							
						</div>
					</div>
				</div>
			</div>
		</div>
		
	</div>
    <script>
      jQuery(document).ready(function($){
         
        $(".sub-menu").hover(function(){
           $(this).parent().css("text-shadow","none !important");
        });
        console.log("execution");
      });
   </script>