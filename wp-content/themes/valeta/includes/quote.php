<?php
/*-----------------------------------------------------------------------------------*/
/*	Post type: Quote
/*-----------------------------------------------------------------------------------*/
?>
	<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		
		<!-- Quote -->		
		<div class="entry-quote">
			<blockquote><p><i class="icon-quote-left"></i> &nbsp;&nbsp;&nbsp;<?php if (get_field('the_quote')) { the_field('the_quote'); } else { echo "Place your awesome quote in the quote field in the post format settings box"; } ?>&nbsp;&nbsp;&nbsp; <i class="icon-quote-right"></i></p></blockquote>
		</div>				 		
	
	   	<div class="entry drop-shadow curved ">
	   		<!-- Title -->
	   		<?php if (get_the_title()) { ?>
	   		<h5 class="heading">
	   			<a href="<?php the_permalink(); ?>">
	   				<?php the_title(); ?>
	   			</a>
	   		</h5>
	   		
	   		<?php if(get_post_type() == "post") { ?>
			   	<div class="post-meta">
	   				By <?php the_author_posts_link(); ?> <?php if (has_category()) { ?>in<?php } ?> <?php the_category(', ') ?> 
	   			</div>
			<?php } ?>
			
			<?php if(get_post_type() == "post") { ?>
			   	<div class="isotope-post-meta">
	   				<?php echo zt_themeblvd_time_ago2(); ?>  
	   			</div>
			<?php } ?>
			
			<?php if(get_post_type() == "my_portfolio") { ?>
			   	<div class="post-meta">
	   				<?php echo zt_themeblvd_time_ago2(); ?> 
	   			</div>
			<?php } ?>
	   		
	   		<div class="post-icons">
	   			<span class="time-ago">
	   			<?php echo zt_themeblvd_time_ago2(); ?>
	   			</span> 
				<?php echo getPostLikeLink(get_the_ID());?> 
				<a href="<?php the_permalink(); ?>" class="post-comment">
					<i class="icon-comment"></i> 
					<?php comments_number('0','1','%'); ?>
				</a>
			</div>

	   		<!-- Icons -->
	   		<?php if(get_field('post_icons') && (get_field('post_type') !== "Slider")) { ?>
	   		<ul class="social">
		   		<?php while(has_sub_field('post_icons')) { ?>
				<li class="social-button-team right"><a href="<?php the_sub_field('post_icon_url')?>" rel="alternate" data-original-title="<?php the_sub_field('post_icon_title')?>"><img height="20" src="<?php echo get_template_directory_uri(); ?>/img/icons/<?php the_sub_field('post_icon')?>" alt="" /></a></li>
				<?php } ?>
		   	</ul>
		   	<?php } ?>
		   	<?php } ?>

	   		<!-- Content -->
	   		<?php if(get_post_type() == "post") { ?><?php if (!is_single()) { the_excerpt(''); } else { the_content(); } ?><?php } ?>
	   		<div class="clearboth"></div>
		</div>	
											
	</div>