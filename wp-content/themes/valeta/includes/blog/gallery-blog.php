<?php
/*-----------------------------------------------------------------------------------*/
/*	Post type: Gallery
/*-----------------------------------------------------------------------------------*/
?>

	<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	
		<?php if( get_field('featured_stamp') ) { ?>
		<div class="featured-stamp">
			<p>FEATURED</p>
		</div>	
		<?php } ?>
				
		<div class="widget-slider">
			<div class="flexslider">
				<ul class="slides">
					
					<?php if (get_field('slider_images')) { ?>
					<?php while(has_sub_field('slider_images')) { ?>
		    		<li>
		    			<div class="entry-image">
		    				<a href="<?php fancybox_url(get_field('image')); ?>" class="<?php fancybox(); ?> entry-image">
		    					<img src="<?php the_sub_field('slider_image'); ?>" alt="" />
		    				</a>
		    			</div>
					</li>
					<?php } ?>
					<?php } else { ?>
					
					<li>
		    			<div class="entry-image">
		    				<a href="<?php fancybox_url(get_field('image')); ?>" class="<?php fancybox(); ?> entry-image">
		    					<img src="http://placehold.it/306x300">
		    		
		    				</a>
		    			</div>
					</li>
					
					<li>
		    			<div class="entry-image">
		    				<a href="<?php fancybox_url(get_field('image')); ?>" class="<?php fancybox(); ?> entry-image">
		    					<img src="http://placehold.it/306x300">
		    		
		    				</a>
		    			</div>
					</li>
					
					<li>
		    			<div class="entry-image">
		    				<a href="<?php fancybox_url(get_field('image')); ?>" class="<?php fancybox(); ?> entry-image">
		    					<img src="http://placehold.it/306x300">
		    		
		    				</a>
		    			</div>
					</li>
					<?php } ?>
		
				</ul>
			</div>
		</div>		
								
	   	<div class="entry drop-shadow curved ">
	   	
	   		<!-- Title -->
	   		<?php if (get_the_title()) { ?>
	   		<h5 class="heading">
	   			<a href="<?php the_permalink(); ?>">
	   				<?php the_title(); ?>
	   			</a>
	   		</h5>
	   		
	   		<?php if(get_post_type() == "post") { ?>
			   	<div class="post-meta">
	   				By <?php the_author_posts_link(); ?> <?php if (has_category()) { ?>in<?php } ?> <?php the_category(', ') ?> 
	   			</div>
			<?php } ?>
			
			<?php if(get_post_type() == "post") { ?>
			   	<div class="isotope-post-meta">
	   				<?php echo zt_themeblvd_time_ago2(); ?>  
	   			</div>
			<?php } ?>
			
			<?php if(get_post_type() == "my_portfolio") { ?>
			   	<div class="post-meta">
	   				<?php echo zt_themeblvd_time_ago2(); ?> 
	   			</div>
			<?php } ?>
	   		
	   		<div class="post-icons">
	   			<span class="time-ago">
	   			<?php echo zt_themeblvd_time_ago2(); ?>
	   			</span> 
				<?php echo getPostLikeLink(get_the_ID());?> 
				<a href="<?php the_permalink(); ?>" class="post-comment">
					<i class="icon-comment"></i> 
					<?php comments_number('0','1','%'); ?>
				</a>
			</div>

	   		<!-- Icons -->
	   		<?php if(get_field('post_icons') && (get_field('post_type') !== "Slider")) { ?>
	   		<ul class="social">
		   		<?php while(has_sub_field('post_icons')) { ?>
				<li class="social-button-team right"><a href="<?php the_sub_field('post_icon_url')?>" rel="alternate" data-original-title="<?php the_sub_field('post_icon_title')?>"><img height="20" src="<?php echo get_template_directory_uri(); ?>/img/icons/<?php the_sub_field('post_icon')?>" alt="" /></a></li>
				<?php } ?>
		   	</ul>
		   	<?php } ?>
		   	<?php } ?>
	   		
	   		<!-- Content -->
	   		<?php if(get_post_type() == "post") { ?><?php if (!is_single()) { the_excerpt(''); } else { the_content(); } ?><?php } ?>
	   		<div class="clearboth"></div>
   		 	
		</div>									
	</div>