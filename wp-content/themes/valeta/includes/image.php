<?php
/*-----------------------------------------------------------------------------------*/
/*	Post type: Image
/*-----------------------------------------------------------------------------------*/
?>
	<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	
		<?php if( get_field('featured_stamp') ) { ?>
		<div class="featured-stamp">
			<p>FEATURED</p>
		</div>	
		<?php } ?>
	
		<?php if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it. ?>
		<?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
		<div class="entry-image">
			<span class="entry-image-overlay"></span>
			<div class="view">
				<a href="<?php fancybox_url(get_field('image')); ?>" class="<?php fancybox(); ?> icon-picture"></a>
				<a href="<?php the_permalink(); ?>" class="icon-external-link"></a>
			</div>					
			<?php the_post_thumbnail('medium');  
         
         ?>
		</div>

		<?php } else { ?>

		<div class="entry-image">
			<span class="entry-image-overlay"></span>
			<div class="view">
				<a href="<?php fancybox_url(get_field('image')); ?>" class="<?php fancybox(); ?> icon-picture"></a>
				<a href="<?php the_permalink(); ?>" class="icon-external-link"></a>
			</div>
			<?php if (get_field('image')) { ?>
			<img src="<?php the_field('image') ?>" alt="" />
			<?php } else { ?>
			<img src="http://placehold.it/306x200">
			<?php } ?>
		</div>
		<?php } ?>

	   	<div class="entry drop-shadow curved ">
	   	
	   		<!-- Title -->
	   		<h1 class="heading">
	   			<a href="<?php the_permalink(); ?>">
	   				<?php the_title(); ?>
	   			</a>
	   		</h1>
            
		   	
	   		<?php if(get_post_type() == "post") { ?>
			   	<div class="post-meta">
	   				By <?php the_author_posts_link(); ?> <?php if (has_category()) { ?>in<?php } ?> <?php the_category(', ') ?> 
	   			</div>
			<?php } ?>
			
			<?php if(get_post_type() == "post") { ?>
			   	<div class="isotope-post-meta">
	   				<?php echo zt_themeblvd_time_ago2(); ?>  
	   			</div>
			<?php } ?>
			
			<?php if(get_post_type() == "portfolio") { ?>
			   	<div class="post-meta">
	   				<?php echo zt_themeblvd_time_ago2(); ?> 
	   			</div>
			<?php } ?>

	   		<?php if(get_post_type() == "post") { ?><?php if (!is_single()) { the_excerpt(''); } else { the_content(); } ?><?php } ?>
	   		<div class="clearboth"></div>
	   		
	   		<div class="post-icons">
	   			<span class="time-ago">
	   			<?php echo zt_themeblvd_time_ago2(); ?>
	   			</span> 
				<?php echo getPostLikeLink(get_the_ID());?> 
				<a href="<?php the_permalink(); ?>" class="post-comment">
					<i class="icon-comment"></i> 
					<?php comments_number('0','1','%'); ?>
				</a>
			</div>
            
		</div>
											
	</div>