<?php get_header();?>
<?php featured_header(); ?>

<div class="page-callout">
	<div class="container">
		<div class="row">
			<div class="span6">
				<?php the_breadcrumb(); ?>
			</div>
			
			<div class="span6">
				<div class="page-name">
					<?php the_title(); ?>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="content-container">
	<div class="container">
		<div class="row">
			<div class="span8">
				<?php while ( have_posts() ) : the_post(); ?>
						
				<?php global $more; $more = 0; ?>
					
				<div class="classic">
					<?php 
               get_template_part('includes/'.'standard');
               /*if(!get_post_format()) {
						get_template_part('includes/'.'standard');
					} else {
						get_template_part('includes/'.get_post_format());
					}*/
					?>
				</div>
						
				<?php endwhile; // end of the loop. ?>

			</div>	
				
			<div class="span4">
				<?php the_content(); ?>
			</div>
				
		</div>
	
	</div> <!--END .container --> 
	<div class="clearboth"></div>
</div>
<?php get_footer(); ?>