<?php get_header();?>
<?php featured_header(); 

?>

<div class="content-container">
	<!-- Grid -->
	<div class="container">
		<div class="isotope row">
							
			<?php
			$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
			
         /*
         query_posts(array('post_type'=>'post', 'paged' => $paged, 'posts_per_page' => get_option('posts_per_page')));
         */
         
         $array_cat = (get_query_var('cat'))? array(get_query_var('cat')): array(get_cat_ID("events"),get_cat_ID("news"));
         
         
         global $wp_query;
         $args = array( 
                  'post_type'=>'post',
                  'paged' => $paged,
                  'posts_per_page' => get_option('posts_per_page'),
                  'category__in' => $array_cat,
                  );
         
         $wp_query = new WP_Query($args);
         
         
			?>
	
			<?php while ( have_posts() ) : the_post(); ?>
	
			<?php
			global $more;
			$more = 0;
			?>
							
			<div class="isotope-post <?php get_category_slug(); ?> <?php get_custom_category_slug($post->ID, 'portfolio_categories'); ?> span6">
				<?php  if(!get_post_format()) {
					get_template_part('includes/blog/standard-blog');
				} else {
					get_template_part('includes/blog/'.get_post_format()."-blog");
				}
				?>
			</div>
			
			<?php endwhile; // end of the loop. ?>
				
		</div>
	</div>
	
	<?php if (get_next_posts_link()) { ?>
		<div class="isotope-loadmore"><?php next_posts_link('<i class="icon-plus"></i> &nbsp;Load more', ''); ?></div>
	<?php } ?>
</div>

<div class="clearboth"></div>
<?php wp_reset_query(); ?>

<?php get_footer(); ?>   