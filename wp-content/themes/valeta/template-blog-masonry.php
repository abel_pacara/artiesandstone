<?php
/*
Template Name: Blog Masonry
*/
?>
<?php get_header();?>
<?php featured_header(); ?>

<div class="page-callout">
	<div class="container">
		<div class="row">
			<div class="span6">
				<?php the_breadcrumb(); ?>
			</div>
			
			<div class="span6">
				<div class="page-name">
					<?php the_title(); ?>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="content-container">
<!-- Grid -->
<div class="container">

	<div class="isotope-preloader"></div>
	<div class="isotope row">
						
		<?php
		$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
		query_posts(array('post_type'=>'post', 'paged' => $paged, 'posts_per_page' => get_option('posts_per_page')));
		?>

		<?php while ( have_posts() ) : the_post(); ?>

		<?php
		global $more;
		$more = 0;
		?>
						
		<div class="isotope-post <?php get_category_slug(); ?> <?php get_custom_category_slug($post->ID, 'portfolio_categories'); ?> span4">
			<?php  if(!get_post_format()) {
				get_template_part('includes/'.'standard');
			} else {
				get_template_part('includes/'.get_post_format());
			}
			?>
		</div>
		
		<?php endwhile; // end of the loop. ?>
			
	</div>
</div>

<?php if (get_next_posts_link()) { ?>
	<div class="isotope-loadmore"><?php next_posts_link('<i class="icon-plus"></i> &nbsp;Load more', ''); ?></div>
<?php } ?>
</div>

<div class="clearboth"></div>
<?php wp_reset_query(); ?>
<?php get_footer(); ?>    