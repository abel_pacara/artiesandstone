<?php get_header();?>
<?php featured_header(); ?>

<div class="page-callout">
	<div class="container">
		<div class="row">
			<div class="span6">
				<?php the_breadcrumb(); ?>
			</div>
			
			<div class="span6">
				<div class="page-name">
					<?php the_title(); ?>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="content-container">
	<div class="container">
		<div class="row">
			<div class="span8">
				<?php while ( have_posts() ) : the_post(); ?>
						
				<?php global $more; $more = 0; ?>
					
				<div class="classic">
					<?php if(!get_post_format()) {
						get_template_part('includes/'.'standard');
					} else {
						get_template_part('includes/'.get_post_format());
					}
					?>
				</div>
						
				<?php endwhile; // end of the loop. ?>
							
				<!-- Tags -->
				<?php if (has_tag()) { ?>
				<div class="tag-cloud">
					 <?php  the_tags('', ' ', '<br />');  ?>
				</div>
				<?php } ?>
					
				<!-- Link Pages -->
				<div class="post-pages">
				<?php custom_wp_link_pages(); ?>
				</div>

				<?php $comment_count = get_comment_count($post->ID); ?>
		 		<?php if ($comment_count['approved'] > 0) : ?>
		 		<?php if (comments_open()) { ?>

				<h3 class="heading"><?php comments_number(); ?></h3>

				<?php } ?>
				<?php endif; ?>
					
				<?php comments_template(); ?>

			</div>	
				
			<div class="span4">
				<?php dynamic_sidebar('post-widgets'); ?>
			</div>
				
		</div>
	
	</div> <!--END .container --> 
	<div class="clearboth"></div>
</div>
<?php get_footer(); ?>